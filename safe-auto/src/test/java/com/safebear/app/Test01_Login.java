package com.safebear.app;

import com.safebear.app.pages.WelcomePage;
import com.safebear.app.utils.CSVImport;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Test Log on page.
 */

public class Test01_Login extends BaseTest {
    CSVImport csvImporting;
@Test
    public void testLogin (){
    csvImporting = new CSVImport();
    csvImporting.csvImport();
//    Step 1 - Confirm we are on the Welcome Page.
    assertTrue(welcomePage.checkCorrectPage());
//    Step 2 - Click on Log in link.
    assertTrue(welcomePage.clickOnLogin(this.loginPage));
//    Step 3 - Log in with correct details.
    assertTrue(loginPage.login(this.userPage,csvImporting.uname,csvImporting.pass));
}
}
