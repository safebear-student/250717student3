package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by cca_student on 25/07/2017.
 */
public class WelcomePage {
    WebDriver driver;
    String hello;
    @FindBy(linkText = "Login")
    WebElement loginLink1;
    @FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[2]/a")
    WebElement loginLink2;
    public Boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Welcome");
    }
    public WelcomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
    public boolean clickOnLogin(LoginPage loginPage){
        loginLink2.click();
        return loginPage.checkCorrectPage();
    }
}
