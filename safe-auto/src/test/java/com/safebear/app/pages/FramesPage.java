package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by cca_student on 25/07/2017.
 */
public class FramesPage {
    WebDriver driver;
    public FramesPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
    public boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Frame");
    }
}
